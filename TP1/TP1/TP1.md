**1ere solution** 

```sudo rm -rf *```

Cette commande supprime tous les fichiers dont ceux du systeme qui permettent de reboot le systeme.

**2eme solution**

Je lance en auto-execution quand l'ordinateur est déverrouillé une suite de commande pour retirer l'accès aux périphériques et donc l'ordinateur est inutilisable.

Pour les périphériques :

```xinput --list```

Permet de récupérer l'id des périphériques.

```xinput set-int-prop id "Device Enabled" 8 0```

Permet de retirer les accès aux périphériques (souris, clavier...)

Pour l'auto exécution :

Ajouter dans le logiciel 'session & startup' les lignes de commandes que j'ai mis dans un fichier .sh.

```chmod +x fichier.sh```

Pour donner les droits d'exécution du fichier afin qu'il s'exécute automatiquement.

**3eme solution**

```shutdown -h now```

J'exécute automatiquement à chaque allumage la commande et cela coupe la machine directement après l'allumage donc l'accès à la machine est impossible.

**4eme solution**

```
sudo rm -r /root
sudo rm -r /lib
sudo rm -r /lib64
exit
```
