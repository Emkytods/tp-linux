Partie 1 : Mise en place et maîtrise du serveur Web

🌞 Installer le serveur Apache

```
[root@localhost ~]# sudo dnf install httpd
```

🌞 Démarrer le service Apache

```
[root@localhost ~]# sudo systemctl start httpd
```

```
[root@localhost ~]# sudo systemctl is-enabled httpd
```

```
[root@localhost ~]# sudo ss -alnpt
```

🌞 TEST

```
[root@localhost ~]# curl localhost
```

2. Avancer vers la maîtrise du service

🌞 Le service Apache...

```
[root@localhost ~]$ cat /usr/lib/systemd/system/httpd.service```
```

🌞 Changer l'utilisateur utilisé par Apache

```
[root@localhost etc]$ cat /etc/passwd | grep user
```

```
[root@localhost conf]$ cat httpd.conf | grep user
```

```
[root@localhost ~]$ ps -ef | grep httpd
```

🌞 Faites en sorte que Apache tourne sur un autre port

```
[root@localhost conf]$ cat httpd.conf | grep -i listen
```

```
[root@localhost ~]$ sudo ss -alntp | grep httpd
```

Partie 2 : Mise en place et maîtrise du serveur de base de données

🌞 Install de MariaDB sur db.tp5.linux

```
[user@localhost ~]$ sudo dnf install mariadb-server
```

```
[user@localhost ~]$ sudo systemctl enable mariadb
```

```
[user@localhost ~]$ sudo systemctl start mariadb
```

```
[user@localhost ~]$ sudo mysql_secure_installation
```

🌞 Port utilisé par MariaDB

```
[user@localhost ~]$ sudo ss -altnp | grep mariadb
```

```
[user@localhost ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
```

🌞 Processus liés à MariaDB

```
[user@localhost ~]$ ps -ef | grep mariadb
```

Partie 3 : Configuration et mise en place de NextCloud

🌞 Préparation de la base pour NextCloud

```
[user@localhost ~]$ sudo mysql -u root -p
```

```
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.105.1.11' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.006 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.105.1.11';
Query OK, 0 rows affected (0.004 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.002 sec)
```

🌞 Exploration de la base de données

```
[user@localhost~]$ mysql -u nextcloud -h 10.105.1.12 -p
```

```
[user@localhost ~]$ sudo dnf install mysql-8.0.30-3.el9_0.x86_64
```

🌞 Install de PHP

```
[user@localhost ~]$ sudo dnf config-manager --set-enabled crb
```

```
[user@localhost ~]$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
```

```
[user@localhost ~]$ sudo dnf module enable php:remi-8.1 -y
```

```
[user@localhost ~]$ sudo dnf install -y php81-php
```

🌞 Install de tous les modules PHP nécessaires pour NextCloud

```
[user@localhost ~]$ sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
```

🌞 Récupérer NextCloud

```
[user@localhost ~]$ curl https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip -O
```

```
[user@localhost ~]$ unzip nextcloud-25.0.0rc3.zip
```

```
[user@localhost ~]$ sudo chown -R apache:apache tp5_nextcloud/
```

🌞 Adapter la configuration d'Apache

```
[user@localhost ~]$ cat /etc/httpd/conf/httpd.conf | grep "\.d"
```

🌞 Redémarrer le service Apache pour qu'il prenne en compte le nouveau fichier de conf

```
[user@localhost ~]$ sudo systemctl restart httpd
```

3. Finaliser l'installation de NextCloud

🌞 Exploration de la base de données

```
[user@localhost ~]$ mysql -u nextcloud -h 10.105.1.12 -p
```