TP6 : Travail autour de la solution NextCloud

Module 1 : Reverse Proxy

🌞 On utilisera NGINX comme reverse proxy

```
[user@localhost ~]$ sudo dnf install -y nginx bind-utils nmap nc tcpdump vim traceroute nano dhclient
```

```
[user@localhost ~] sudo systemctl start nginx 
```

```
[user@localhost ~] sudo systemctl status nginx 
```

```
[user@localhost ~]$ sudo ss -alnpt
```

```
[user@localhost ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
```

```
[user@localhost ~]$ sudo ps -ef | grep nginx
```

```
[user@localhost ~]$ curl http://localhost:80
```

🌞 Configurer NGINX

```
[user@localhost ~]$ cat nginx.conf | grep proxy_pass
```

```
[user@localhost ~]$ sudo cat config.php | grep trusted
```

🌞 Faites en sorte de

```
[user@localhost ~]$ sudo firewall-cmd --zone=public --add-rich-rule='rule family="ipv4" source address="10.105.1.13/32" invert="True" drop' --permanent
```

🌞 Une fois que c'est en place

```
PS C:\Users\thomc> ping 10.105.1.13
```

```
PS C:\Users\thomc> ping 10.105.1.11
```

II.HTTPS

🌞 Faire en sorte que NGINX force la connexion en HTTPS plutôt qu'HTTP

```
[user@localhost ~]$ sed '41,42!d' nginx.conf
```

Module 2 : Sauvegarde du système de fichiers

🌞 Ecrire le script bash

```
cd /srv
mkdir backup
cd /backup
heure="$(date +%H%M)"
jour="$(date +%y%m%d)"
zip -r "nextcloud_"${jour}""${heure}"" /home/user/nextcloud/core/Db /home/user/nextcloud/core/Data /home/user/nextcloud/config /home/user/nextcloud/themes
cd /srv
mv nextcloud* /srv/backup
```

🌞 Créez un service système qui lance le script

```
[user@localhost system]$ sudo systemctl status backup | grep Jan
```

🌞 Créez un timer système qui lance le service à intervalles réguliers

```
[user@localhost system]$ cat backup.timer
```

🌞 Activez l'utilisation du timer

```
[user@localhost system]$ sudo systemctl list-timers | grep backup
```

🌞 Préparer un dossier à partager sur le réseau(sur la machine storage.tp6.linux)

```
[user@storage ~]$ sudo mkdir /srv/nfs_shares
```

```
[user@localhost ~]$ sudo mkdir /srv/nfs_shares/web.tp6.linux
```

🌞 Installer le serveur NFS (sur la machine storage.tp6.linux)

```
[user@localhost ~]$ sudo yum update
```

```
[user@storage ~]$ sudo dnf install nfs-utils -y
```

```
[user@storage ~]$ sudo nano /etc/exports
```

```
[user@storage ~]$ sudo firewall-cmd --permanent --add-service=nfs
```

```
[user@storage ~]$ sudo firewall-cmd --permanent --add-service=mountd
```

```
[user@storage ~]$ sudo firewall-cmd --permanent --add-service=rpc-bind
```

```
[user@storage ~]$ sudo firewall-cmd --reload
```

```
[user@storage ~]$ sudo firewall-cmd --list-all | grep services
```

```
[user@storage ~]$ sudo systemctl start nfs-server
```

```
[user@storage ~]$ sudo systemctl status nfs-server | grep Active
```

🌞 Installer un client NFS sur web.tp6.linux

```
[user@web ~]$ sudo mount 10.105.1.14:/srv/nfs_shares/web.tp6.linux/ /srv/backup/
```

Module 4 : Monitoring

🌞 Installer Netdata

```
[user@web ~]$ sudo dnf install epel-release -y
```

```
[user@web ~]$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
```

```
[user@web ~]$ sudo systemctl start netdata
```

```
[user@web ~]$ sudo systemctl enable netdata
```

```
[user@web ~]$ sudo systemctl status netdata | grep Active
```

```
[user@web ~]$ sudo firewall-cmd --permanent --add-port=19999/tcp
```

```
[user@web ~]$ sudo firewall-cmd --reload
```

🌞 Une fois Netdata installé et fonctionnel, déterminer :

```
[user@web ~]$ ps -ef | grep netdata | head -n5 | tail -n-1
```

```
[user@web ~]$ sudo ss -ltpnu | grep netdata
```

🌞 Configurer Netdata pour qu'il vous envoie des alertes

```
[user@web ~]$ sudo nano /etc/netdata/health.d/cpu.conf
```

```
[user@web ~]$ sudo nano /etc/netdata/health.d/ram-usage.conf
```

```
[user@web ~]$ sudo cat /etc/netdata/health_alarm_notify.conf | tail -n 7
```

🌞 Vérifier que les alertes fonctionnent

```
[user@web ~]$ sudo dnf install epel-release
```

```
[user@web ~]$ sudo dnf install stress
```

```
[user@web ~]$ sudo stress --cpu 8 --vm 2 --vm-bytes 2048M --timeout 10s
```

Module 3 : Fail2Ban

```
[user@db ~]$ sudo dnf install epel-release -y
```

```
[user@db ~]$ sudo dnf install fail2ban -y
```

```
[user@db ~]$ cd /etc/fail2ban
```

```
[user@db fail2ban]$ head -20 jail.conf
```

```
[user@db fail2ban]$ sudo cp jail.conf jail.local
```

```
[user@db fail2ban]$ sudo nano jail.local
```

```
[user@db fail2ban]$ sudo systemctl enable --now fail2ban
```

```
[user@db fail2ban]$ sudo systemctl start fail2ban
```

```
[user@db fail2ban]$ sudo systemctl status fail2ban
```